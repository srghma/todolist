source 'https://rubygems.org'

gem 'rails', '~> 5.0.0', '>= 5.0.0.1'
gem 'pg'
gem 'puma', '~> 3.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'jquery-rails'
gem 'turbolinks', '~> 5'
gem 'active_model_serializers'

gem 'bootstrap-sass'
gem 'slim-rails'
# second login is failing, better do this in js
# gem 'angular_rails_csrf', github: "AlanDonohoe/angular_rails_csrf"
gem 'angular-rails-templates'
gem 'devise'
gem 'omniauth-facebook'
gem 'acts_as_list'

# File uploader
gem 'carrierwave'
gem 'mini_magick'

# AngularJS
source 'https://rails-assets.org' do
  gem 'rails-assets-angular'
  gem 'rails-assets-ui-router'
  gem 'rails-assets-moment'
  gem 'rails-assets-angular-bootstrap'
  gem 'rails-assets-angular-bootstrap-datetimepicker'
  gem 'rails-assets-angular-resource'
  gem 'rails-assets-angular-animate'
  gem 'rails-assets-angular-loading-bar'
  gem 'rails-assets-angular-elastic'
  gem 'rails-assets-ng-sortable'
  gem 'rails-assets-ng-file-upload'
  gem 'rails-assets-lodash'
end

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'sqlite3'

  # RSpec
  gem 'rspec-rails'
  gem 'shoulda'
  gem 'shoulda-matchers', github: 'thoughtbot/shoulda-matchers'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'database_cleaner'
  gem 'fuubar'
  gem 'airborne' # api  testing

  # Acceptance
  gem 'capybara'
  gem 'poltergeist'
  gem 'launchy'
end

group :development do
  gem 'annotate'
  gem 'awesome_pry'
  gem 'better_errors'
  gem 'guard-livereload'

  gem 'web-console'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
