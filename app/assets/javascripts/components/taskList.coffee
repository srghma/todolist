angular.module('app').component 'taskList',
  templateUrl: 'taskList.html'
  controller: 'TaskCtrl'
  bindings:
    project: '='
    onTaskClick: '&'
