angular.module('app').controller 'AuthCtrl', [ '$scope', 'Auth', '$uibModalInstance', '$state', '$window'
    , ($scope, Auth, $uibModalInstance, $state, $window) ->
  capitalise = (str) -> str.charAt(0).toUpperCase() + str[1..]

  this.register = ->
    Auth.register
      email: $scope.email,
      password: $scope.password,
      password_confirmation: $scope.password
    .then (registeredUser)->
      do $uibModalInstance.close
    .catch (reason)->
      console.log reason
      $scope.errors = []
      for key, value of reason.data.errors
        key = capitalise(key)
        full_message = "#{key} #{value}."
        $scope.errors.push(full_message)

  this.login = ->
    Auth.login
      email: $scope.email,
      password: $scope.password
    .then (resp)->
      do $uibModalInstance.close
    .catch (reason)->
      $scope.errors = [reason.data.error]
      console.log reason

  this.facebook = ->
    facebookPath = "/api/users/auth/facebook"
    omniauthWindowType = "sameWindow"
    authUrl = buildFacebookUrl(omniauthWindowType, facebookPath, { json: true })
    authWindow = $window.location.replace(authUrl)

  buildFacebookUrl = (omniauthWindowType, facebookPath, opts={}) ->
    authUrl  = window.location.origin
    authUrl += facebookPath
    authUrl += '?auth_origin_url=' + encodeURIComponent(window.location.href)

    params = angular.extend({}, opts.params || {}, {
      omniauth_window_type: omniauthWindowType
    })

    for key, val of params
      authUrl += '&'
      authUrl += encodeURIComponent(key)
      authUrl += '='
      authUrl += encodeURIComponent(val)

    return authUrl

  return
]