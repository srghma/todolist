app = angular.module 'app'

app.config [ "$stateProvider", "$urlRouterProvider", ($stateProvider, $urlRouterProvider) ->
  $urlRouterProvider.otherwise "/"

  $stateProvider
    .state "index",
      url: "/"
      templateUrl: 'projectList.html'
      controller: 'ProjectCtrl'
      controllerAs: '$ctrl'
      resolve:
        authenticated: ["Auth", "$state", (Auth, $state) ->
          Auth.currentUser().catch ->
            $state.go("auth")
        ]

    .state "auth",
      url: "/auth"
      onEnter: ['$state', '$uibModal', "Auth", ($state, $uibModal, Auth) ->
        Auth.currentUser().then( ->
          $state.go('index')
        , ->
          $uibModal.open
            templateUrl: "auth_modal.html"
            controller: "AuthCtrl"
            controllerAs: "$ctrl"
            backdrop  : 'static'
            keyboard  : false
          .result.finally ()->
            $state.go('index')
        )
      ]
]
