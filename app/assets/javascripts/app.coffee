app = angular.module 'app', [
  'templates'
  'ui.router'
  'ui.bootstrap'
  'ui.bootstrap.datetimepicker'
  'ngResource'
  'ngAnimate'
  'angular-loading-bar'
  'monospaced.elastic'
  'as.sortable'
  'ngFileUpload'
  'Devise'
]

app.config [ "$locationProvider", "AuthProvider", ($locationProvider, AuthProvider) ->
  $locationProvider.html5Mode(true)
  AuthProvider.baseUrl(ROOT_URL + 'api')
]

app.config ['cfpLoadingBarProvider', (cfpLoadingBarProvider) ->
  cfpLoadingBarProvider.includeSpinner = false
  cfpLoadingBarProvider.latencyThreshold = 500
]

app.config ['$httpProvider', ($httpProvider) ->
  token = $('meta[name=csrf-token]').attr 'content'

  $httpProvider.defaults.headers.common['X-CSRF-Token'] = token
]

app.run [ "$rootScope", "Auth", '$state', ($rootScope, Auth, $state) ->
  # Auth.login()
  $rootScope.isAuthenticated = Auth.isAuthenticated()
  $rootScope.currentUser = Auth._currentUser

  $rootScope.$watch(
    () -> return Auth.isAuthenticated()
  , (newValue, oldValue) ->
    if newValue != oldValue
      $rootScope.isAuthenticated = newValue
      $rootScope.currentUser = Auth._currentUser
  )

  # Catch unauthorized requests and recover.
  $rootScope.$on 'devise:unauthorized', (event, xhr, deferred) ->
    $state.go('auth')

  $rootScope.$on 'devise:logout', (event, oldCurrentUser)->
    $state.go('auth')
]