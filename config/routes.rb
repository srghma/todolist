Rails.application.routes.draw do
  scope "api", defaults: { format: :json } do
    devise_for :users, :controllers => { :omniauth_callbacks => "omniauth_callbacks" }

    resources :projects, defaults: { format: :json }

    resources :tasks, defaults: { format: :json } do
      put :done, on: :member
      put :sort, on: :member
      put :deadline, on: :member
    end

    resources :comments, defaults: { format: :json }, only: [:create, :destroy]

    resources :attachments, defaults: { format: :json }, only: [:create]
  end


  root 'application#angular'
  get "/auth" => "application#angular"

end
